/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        ListNode *current=head;
        ListNode *Next=NULL, *prev=NULL;
        
        while(current)
        {
            Next = current->next;
            current->next = prev;
            prev = current;
            current = Next;
            
        }
        head = prev;
        return head;
    }
   
};