/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
   
    ListNode *removeNthFromEnd(ListNode *head, int n) {
    
    ListNode *pre, *cur;
    pre = cur = head;
    for (int i = 0; i < n; ++i) {
      pre = pre->next;
    }

    while (pre != NULL && pre->next != NULL) {
      cur = cur->next;
      pre = pre->next;
    }
    if (pre == NULL) 
    {
      ListNode *newHead = head->next;
      delete head;
      return newHead;
    }
    else 
    {
      cur->next = cur->next->next;
      return head;
    }        
}
};