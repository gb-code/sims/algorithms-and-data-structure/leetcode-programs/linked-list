/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* mid(ListNode* head) {
        ListNode *p1=head, *p2=head;
        while(p2 && p2->next){
            p1 = p1->next;
            p2 = p2->next->next;
        }
        return p1;
    }
    
    ListNode* reverse(ListNode* head) {
        ListNode* p1=NULL;
        
        while (head) {
            ListNode* p2 = head->next;
            head->next = p1;
            p1 = head;
            head = p2;
        }
        return p1;
    }
    
    bool isPalindrome(ListNode* head) {
        ListNode* pmid = mid(head);
        ListNode* prev = reverse(pmid); 
        for(;head!=pmid; head=head->next, prev=prev->next) {
            if (head->val != prev->val) {
                return false;
            }
        }
        return true;
    }
};