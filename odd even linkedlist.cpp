/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode* oddEvenList(ListNode* head) {
        if (!head) return head;
        ListNode* podd = head;
        ListNode* peven = head->next;
        ListNode* pnext = NULL;
        while(peven && (pnext=peven->next)) {
            
            peven->next = pnext->next;
            pnext->next = podd->next;
            podd->next = pnext;
            
            peven = peven->next;
            podd = podd->next;
        
        }
        return head;
    }
};