/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        
        ListNode *first = headA, *second = headB;
        int lenA = 0,lenB = 0;
        while(first)
		{
            lenA++;
            first = first->next;
        }
        
        while(second)
		{
            lenB++;
            second = second->next;
        }
        first = headA,second = headB;
        
        while(lenA > lenB)
		{         
            first = first->next;
            lenA--;
        }
        while(lenB > lenA)
		{
            second = second->next;
            lenB--;
        }
        while(first && second)
		{
            if(first == second)    return first;
            else{
                first = first->next;
                second = second->next;
            }
        }
        return 0;
    }
	
	

};